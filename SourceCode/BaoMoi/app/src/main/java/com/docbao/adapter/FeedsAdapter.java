package com.docbao.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.docbao.R;
import com.docbao.model.FeedItem;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by rishabh on 26-02-2016.
 */
public class FeedsAdapter extends RecyclerView.Adapter<FeedsAdapter.MyViewHolder> {
    ArrayList<FeedItem> feedItems;
    Context context;
    OnclickView onclickView;

    public FeedsAdapter(Context context, ArrayList<FeedItem> feedItems,OnclickView onclickView1) {
        this.feedItems = feedItems;
        this.onclickView = onclickView1;
        this.context = context;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.custum_row_news_item, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        YoYo.with(Techniques.FadeIn).playOn(holder.cardView);
        final FeedItem current = feedItems.get(position);
        holder.Title.setText(current.getTitle());
        holder.description.setText(current.getTitle());
        String s = current.getPubDate();
        String date = s.replace("+0700", "");
        holder.Description.setText(date);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              onclickView.OnclickViewItem(current.getLink());
            }
        });



    }


    @Override
    public int getItemCount() {
        return feedItems.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView Title, Description,description;
        RelativeLayout cardView;

        public MyViewHolder(View itemView) {
            super(itemView);
            Title =  itemView.findViewById(R.id.title_text);
            Description =  itemView.findViewById(R.id.description_text);
            description =  itemView.findViewById(R.id.description);
            cardView =  itemView.findViewById(R.id.cardview);
        }
    }
    public interface OnclickView{
        void OnclickViewItem(String link);
    }
}
