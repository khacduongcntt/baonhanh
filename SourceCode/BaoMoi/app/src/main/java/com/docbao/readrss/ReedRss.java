package com.docbao.readrss;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.docbao.model.RssModel;

import org.jsoup.Jsoup;
import org.jsoup.select.Elements;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

public class ReedRss extends AsyncTask<Void, Void, Void> {
    Context context;
    String address = "https://www.24h.com.vn/upload/rss/trangchu24h.rss";
    ProgressDialog progressDialog;
    ArrayList<RssModel> feedItems = new ArrayList<>();
    RecyclerView recyclerView;
    URL url;

    public ReedRss(Context context, RecyclerView recyclerView, String path) {
        this.recyclerView = recyclerView;
        this.context = context;
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Loading...");
    }

    @Override
    protected void onPreExecute() {
        progressDialog.show();
        super.onPreExecute();

    }

    @Override
    protected Void doInBackground(Void... voids) {
        feedItems.clear();
        ProcessXml(Getdata());
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        progressDialog.dismiss();
    }

    String regularExpression = "src=\"(.*)\" class";

    private void ProcessXml(Document data) {
        if (data != null) {

            Element root = data.getDocumentElement();
            Node channel = root.getChildNodes().item(1);
            NodeList items = channel.getChildNodes();
            for (int i = 0; i < items.getLength(); i++) {
                Node cureentchild = items.item(i);
                if (cureentchild.getNodeName().equalsIgnoreCase("item")) {
                    RssModel item = new RssModel();
                    NodeList itemchilds = cureentchild.getChildNodes();
                    for (int j = 0; j < itemchilds.getLength(); j++) {
                        Node cureent = itemchilds.item(j);
                        if (cureent.getNodeName().equalsIgnoreCase("title")) {
                            item.setTitle(cureent.getTextContent());

                        } else if (cureent.getNodeName().equalsIgnoreCase("description")) {
                            item.setDescription(cureent.getTextContent());
                        } else if (cureent.getNodeName().equalsIgnoreCase("pubDate")) {

                            item.setPubDate(cureent.getTextContent());
                        } else if (cureent.getNodeName().equalsIgnoreCase("link")) {

                            item.setLink(cureent.getTextContent());

                        } else if (cureent.getNodeName().equalsIgnoreCase("media:thumbnail")) {


                            String url = cureent.getAttributes().item(0).getTextContent();
                        }


                    }
                    feedItems.add(item);


                }
            }
        }
    }

    //This method will download rss feed document from specified url
    public Document Getdata() {
        try {
            url = new URL(address);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            InputStream inputStream = connection.getInputStream();
            DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = builderFactory.newDocumentBuilder();
            Document xmlDoc = builder.parse(inputStream);
            return xmlDoc;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private String extractImageUrl(String description) {
        String s;
        org.jsoup.nodes.Document document = Jsoup.parse(description);
        Elements imgs = document.select("img");

        for (org.jsoup.nodes.Element img : imgs) {
            if (img.hasAttr("src")) {
                return img.attr("src");
            }
        }

        // no image URL
        return "";
    }
}
