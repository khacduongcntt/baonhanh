package com.docbao.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.docbao.R;
import com.docbao.model.Navmodel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class NavAdapter extends RecyclerView.Adapter<NavAdapter.ViewHolder> {


    Context mContext;
    ArrayList<Navmodel> arrayList;
    OnclickNav onclickNav;

    public NavAdapter(Context mContext, ArrayList<Navmodel> arrayList,OnclickNav onclickNav1) {
        this.mContext = mContext;
        this.arrayList = arrayList;
        this.onclickNav = onclickNav1;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_nav, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {
        viewHolder.txt_title_nav.setText(arrayList.get(i).getTitle());
        Glide.with(mContext).load(arrayList.get(i).getIcon()).into(viewHolder.img_nav);
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onclickNav.ClickNav(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView img_nav;
        TextView txt_title_nav;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            img_nav = itemView.findViewById(R.id.img_icon_nav);
            txt_title_nav = itemView.findViewById(R.id.txt_title_nav);
        }
    }
    public interface OnclickNav {
        void ClickNav(int posison);
    }
}
