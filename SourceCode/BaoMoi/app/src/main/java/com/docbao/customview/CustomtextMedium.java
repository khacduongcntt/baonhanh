package com.docbao.customview;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

@SuppressLint("AppCompatCustomView")
public class CustomtextMedium extends TextView {
    public CustomtextMedium(Context context) {
        super(context);
        settyface(context);
    }

    public CustomtextMedium(Context context,AttributeSet attrs) {
        super(context, attrs);
        settyface(context);
    }

    public CustomtextMedium(Context context,  AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        settyface(context);
    }

    public void settyface(Context context){
        this.setTypeface(Typeface.createFromAsset(context.getAssets(),"Roboto-Medium_1.ttf"));

    }
}
