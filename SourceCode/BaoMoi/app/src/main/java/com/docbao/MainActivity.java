package com.docbao;

import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.docbao.adapter.NavAdapter;
import com.docbao.fragment.PlaceHolderFragment;
import com.docbao.model.Navmodel;
import com.docbao.tablayout.BadgedTabLayout;

import java.util.ArrayList;

import company.librate.RateDialog;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, NavigationView.OnNavigationItemSelectedListener {
    private SectionsPagerAdapter mSectionsPagerAdapter;
    int counter = 1;
    private ViewPager mViewPager;
    RecyclerView recyclerView_nav;
    private BadgedTabLayout tabLayout;
    ArrayList<Navmodel> arrayList_nav = new ArrayList<>();
    DrawerLayout drawer;
    ImageView btn_menu_option;
    RateDialog rateDialog;
    NavAdapter.OnclickNav onclickNav = new NavAdapter.OnclickNav() {
        @Override
        public void ClickNav(int posison) {
            mViewPager.setCurrentItem(posison);
            drawer.closeDrawers();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);
        recyclerView_nav = findViewById(R.id.recryclerview_nav);
        rateDialog = new RateDialog(MainActivity.this, "", true);
        btn_menu_option = findViewById(R.id.btn_menu_option);
        recyclerView_nav.setLayoutManager(new LinearLayoutManager(MainActivity.this, LinearLayoutManager.VERTICAL, false));
         drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, null, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        setadapterNav();
        tabarlayout();
        findViewById(R.id.btn_menu).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer.openDrawer(Gravity.START);
            }
        });

        btn_menu_option.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final PopupMenu popup = new PopupMenu(MainActivity.this, btn_menu_option);
                //Inflating the Popup using xml file
                popup.getMenuInflater().inflate(R.menu.a, popup.getMenu());
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {

                        switch (item.getItemId()) {
                            case R.id.one:
                                rateDialog.show();
                                popup.dismiss();
                                break;
                            case R.id.three:
                                Uri uri = Uri.parse(getString(R.string.policy));
                                Intent intent1 = new Intent(Intent.ACTION_VIEW, uri);
                                startActivity(intent1);
                                popup.dismiss();
                                break;

                            case R.id.two:
                                Intent intent = new Intent();
                                intent.setAction(Intent.ACTION_SENDTO);
                                intent.setData(Uri.parse(getString(R.string.email)));
                                startActivity(Intent.createChooser(intent, "Send feedback"));

                                break;
                        }
                        return true;
                    }
                });
                popup.show();
                //showing popup menu
            }
        });
    }

    private void setadapterNav() {
        arrayList_nav.add(new Navmodel(R.drawable.home, "Trang chủ"));
        arrayList_nav.add(new Navmodel(R.drawable.tintuc, "Thời sự"));
        arrayList_nav.add(new Navmodel(R.drawable.thegioi, "Thế giới"));
        arrayList_nav.add(new Navmodel(R.drawable.kinhdoanh, "Kinh doanh"));
        arrayList_nav.add(new Navmodel(R.drawable.startup, "Startup"));
        arrayList_nav.add(new Navmodel(R.drawable.giaitri, "Giải trí"));
        arrayList_nav.add(new Navmodel(R.drawable.thethao, "Thể thao"));
        arrayList_nav.add(new Navmodel(R.drawable.luatphap, "Pháp luật"));
        arrayList_nav.add(new Navmodel(R.drawable.education, "Giáo dục"));
        arrayList_nav.add(new Navmodel(R.drawable.suckhoe, "Sức khỏe"));
        arrayList_nav.add(new Navmodel(R.drawable.doisong, "Đời sống"));
        arrayList_nav.add(new Navmodel(R.drawable.dulich, "Du lịch"));
        arrayList_nav.add(new Navmodel(R.drawable.khoahoc, "Khoa học"));
        arrayList_nav.add(new Navmodel(R.drawable.sohoa, "Số hóa"));
        arrayList_nav.add(new Navmodel(R.drawable.xe, "Xe"));
        arrayList_nav.add(new Navmodel(R.drawable.ykien, "Ý kiến"));
        arrayList_nav.add(new Navmodel(R.drawable.smile, "Cười"));
        NavAdapter navAdapter = new NavAdapter(MainActivity.this, arrayList_nav, onclickNav);
        recyclerView_nav.setAdapter(navAdapter);

    }

    private void tabarlayout() {

        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);


        tabLayout = findViewById(R.id.tabs);

        tabLayout.setupWithViewPager(mViewPager);

        tabLayout.setIcon(0, R.drawable.ic_home_black_24dp);
//        tabLayout.setIcon(1, R.drawable.ic_shopping);


//        tabLayout.setBadgeText(0, String.valueOf(counter));
//        tabLayout.setBadgeText(2, "13213131");

//        tabLayout.setTabFont(ResourcesCompat.getFont(this, R.font.trench));

        tabLayout.setBadgeTruncateAt(TextUtils.TruncateAt.MIDDLE);

    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        return false;
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).

            PlaceHolderFragment placeHolderFragment = null;
            switch (position) {
                case 0:
                    placeHolderFragment = new PlaceHolderFragment(MainActivity.this, "https://vnexpress.net/rss/tin-moi-nhat.rss", "https://www.24h.com.vn/upload/rss/asiancup2019.rss");
                    break;
                case 1:
                    placeHolderFragment = new PlaceHolderFragment(MainActivity.this, "https://vnexpress.net/rss/thoi-su.rss", "https://www.24h.com.vn/upload/rss/tintuctrongngay.rss");
                    break;
                case 2:
                    placeHolderFragment = new PlaceHolderFragment(MainActivity.this, "https://vnexpress.net/rss/the-gioi.rss", "https://www.24h.com.vn/upload/rss/bongda.rss");
                    break;
                case 3:
                    placeHolderFragment = new PlaceHolderFragment(MainActivity.this, "https://vnexpress.net/rss/kinh-doanh.rss", "https://www.24h.com.vn/upload/rss/thitruongtieudung.rss");
                    break;
                case 4:
                    placeHolderFragment = new PlaceHolderFragment(MainActivity.this, "https://vnexpress.net/rss/startup.rss", "https://www.24h.com.vn/upload/rss/taichinhbatdongsan.rss");
                    break;
                case 5:
                    placeHolderFragment = new PlaceHolderFragment(MainActivity.this, "https://vnexpress.net/rss/giai-tri.rss", "https://www.24h.com.vn/upload/rss/canhacmtv.rss");
                    break;
                case 6:
                    placeHolderFragment = new PlaceHolderFragment(MainActivity.this, "https://vnexpress.net/rss/the-thao.rss", "https://www.24h.com.vn/upload/rss/thethao.rss");
                    break;
                case 7:
                    placeHolderFragment = new PlaceHolderFragment(MainActivity.this, "https://vnexpress.net/rss/phap-luat.rss", "https://www.24h.com.vn/upload/rss/anninhhinhsu.rss");
                    break;
                case 8:
                    placeHolderFragment = new PlaceHolderFragment(MainActivity.this, "https://vnexpress.net/rss/giao-duc.rss", "https://www.24h.com.vn/upload/rss/giaoducduhoc.rss");
                    break;
                case 9:
                    placeHolderFragment = new PlaceHolderFragment(MainActivity.this, "https://vnexpress.net/rss/suc-khoe.rss", "https://www.24h.com.vn/upload/rss/suckhoedoisong.rss");
                    break;
                case 10:
                    placeHolderFragment = new PlaceHolderFragment(MainActivity.this, "https://vnexpress.net/rss/gia-dinh.rss", "https://www.24h.com.vn/upload/rss/amthuc.rss");
                    break;
                case 11:
                    placeHolderFragment = new PlaceHolderFragment(MainActivity.this, "https://vnexpress.net/rss/du-lich.rss", "https://www.24h.com.vn/upload/rss/dulich24h.rss");
                    break;
                case 12:
                    placeHolderFragment = new PlaceHolderFragment(MainActivity.this, "https://vnexpress.net/rss/khoa-hoc.rss", "https://www.24h.com.vn/upload/rss/congnghethongtin.rss");
                    break;
                case 13:
                    placeHolderFragment = new PlaceHolderFragment(MainActivity.this, "https://vnexpress.net/rss/so-hoa.rss", "https://www.24h.com.vn/upload/rss/phithuongkyquac.rss");
                    break;
                case 14:
                    placeHolderFragment = new PlaceHolderFragment(MainActivity.this, "https://vnexpress.net/rss/oto-xe-may.rss", "https://www.24h.com.vn/upload/rss/oto.rss");
                    break;
                case 15:
                    placeHolderFragment = new PlaceHolderFragment(MainActivity.this, "https://vnexpress.net/rss/y-kien.rss", "https://www.24h.com.vn/upload/rss/phim.rss");
                    break;
                case 16:
                    placeHolderFragment = new PlaceHolderFragment(MainActivity.this, "https://vnexpress.net/rss/tam-su.rss", "https://www.24h.com.vn/upload/rss/bantrecuocsong.rss");
                    break;

                case 17:
                    placeHolderFragment = new PlaceHolderFragment(MainActivity.this, "https://vnexpress.net/rss/cuoi.rss", "https://www.24h.com.vn/upload/rss/cuoi24h.rss");
                    break;
            }
            return placeHolderFragment;


        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 18;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Home";
                case 1:
                    return "Thời sự";
                case 2:
                    return "Thế giới";
                case 3:
                    return "Kinh doanh";
                case 4:
                    return "Startup";
                case 5:
                    return "Giải trí";
                case 6:
                    return "Thể thao";
                case 7:
                    return "Pháp luật";
                case 8:
                    return "Giáo dục";
                case 9:
                    return "Sức khỏe";
                case 10:
                    return "Đời sống";
                case 11:
                    return "Du lịch";
                case 12:
                    return "Khoa học";
                case 13:
                    return "Số hóa";
                case 14:
                    return "Xe";
                case 15:
                    return "Ý kiến";
                case 16:
                    return "Tâm sự";
                case 17:
                    return "Cười";


            }
            return null;
        }
    }
    @Override
    public void onBackPressed() {
            if (!rateDialog.isRate()) {
                rateDialog.show();
            } else {
                super.onBackPressed();
            }


    }
}
