package com.docbao.fragment;

/**
 * Created by rahim on 23/08/17.
 */

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.docbao.MainActivity;
import com.docbao.R;
import com.docbao.WebviewActivity;
import com.docbao.adapter.FeedsAdapter;
import com.docbao.adapter.RssItemsAdapter;

import com.docbao.customrecyclerview.ShimmerRecyclerViewX;
import com.docbao.read.RssConverterFactory;
import com.docbao.read.RssFeed;
import com.docbao.read.RssItem;
import com.docbao.readrss.ReadRss;
import com.docbao.readrss.ReedRss;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * A placeholder fragment containing a simple view.
 */
@SuppressLint("ValidFragment")

public class PlaceHolderFragment extends Fragment implements RssItemsAdapter.OnItemClickListener, SwipeRefreshLayout.OnRefreshListener, FeedsAdapter.OnclickView {
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";
    Context context;
    ShimmerRecyclerViewX recyclerView;
    RecyclerView recryclerview_bot;
    String path;
    private RssItemsAdapter mAdapter;
    SwipeRefreshLayout mSwRefresh;
    private Animator spruceAnimator;
    private final List<RssItem> mItems = new ArrayList<>();
    int y;
    LinearLayoutManager layoutManager;
    RelativeLayout rlt_bottom;
    int curtime, totalitem, scolitem;
    String addread;


    public PlaceHolderFragment(Context context, String path,  String addrea2) {
        this.context = context;
        this.path = path;
        this.addread = addrea2;
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
//    public static PlaceHolderFragment newInstance(int sectionNumber, Context context, String path) {
//        PlaceHolderFragment fragment = new PlaceHolderFragment(context, path);
//        Bundle args = new Bundle();
//        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
//        fragment.setArguments(args);
//        return fragment;
//    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        rlt_bottom = rootView.findViewById(R.id.rlt_bottom);
        recyclerView = rootView.findViewById(R.id.recyclerview_fm);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(context,LinearLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(layoutManager);
        mSwRefresh = rootView.findViewById(R.id.swRefresh);
        mAdapter = new RssItemsAdapter(getActivity());
        mAdapter.setListener(this);
        recyclerView.setAdapter(mAdapter);
        recyclerView.showShimmerAdapter();
        mSwRefresh.setOnRefreshListener(this);
        recyclerView.postDelayed(new Runnable() {
            @Override
            public void run() {
                recyclerView.hideShimmerAdapter();
            }
        }, 3000);
        fetchRss();



        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
//                if(recyclerView.SCROLL_STATE_DRAGGING==newState){
//                    //fragProductLl.setVisibility(View.GONE);
//                }
//                if(recyclerView.SCROLL_STATE_IDLE==newState){
//                    // fragProductLl.setVisibility(View.VISIBLE);
//                    if(y<=0){
//                        Toast.makeText(context, "hien", Toast.LENGTH_SHORT).show();
//                    }
//                    else{
//                        y=0;
//                        Toast.makeText(context, "an", Toast.LENGTH_SHORT).show();
//                    }
//                }
                curtime = layoutManager.getChildCount();
                totalitem = layoutManager.getItemCount();
                scolitem = layoutManager.findFirstVisibleItemPosition();
                if ((curtime + scolitem == totalitem)) {
                    rlt_bottom.setVisibility(View.VISIBLE);

                }else if ((curtime + scolitem < totalitem)){
                    rlt_bottom.setVisibility(View.GONE);
                }

            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                y = dy;
            }
        });




        recryclerview_bot = rootView.findViewById(R.id.recryclerview_bot);
        ReadRss readRss = new ReadRss(context, recryclerview_bot,addread);
        readRss.execute();






        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (spruceAnimator != null) {
            spruceAnimator.start();
        }
    }

    @Override
    public void onItemSelected(RssItem rssItem) {
        Intent intent1 = new Intent(context, WebviewActivity.class);
        intent1.putExtra("linkurl", rssItem.getLink());
        startActivity(intent1);
    }

    private void fetchRss() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://github.com")
                .addConverterFactory(RssConverterFactory.create())
                .build();

        showLoading();
        RssService service = retrofit.create(RssService.class);
        service.getRss(path)
                .enqueue(new Callback<RssFeed>() {
                    @Override
                    public void onResponse(Call<RssFeed> call, Response<RssFeed> response) {
                        onRssItemsLoaded(response.body().getItems());
                        hideLoading();
                    }

                    @Override
                    public void onFailure(Call<RssFeed> call, Throwable t) {
                        Toast.makeText(getActivity(), "Failed to fetchRss RSS feed!", Toast.LENGTH_SHORT).show();

                    }
                });
    }

    public void onRssItemsLoaded(List<RssItem> rssItems) {
        mAdapter.setItems(rssItems);
        mItems.addAll(rssItems);
        mAdapter.notifyDataSetChanged();
        if (recyclerView.getVisibility() != View.VISIBLE) {
            recyclerView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onRefresh() {
        fetchRss();
    }

    public void showLoading() {
        mSwRefresh.setRefreshing(true);
    }


    /**
     * Hides {@link SwipeRefreshLayout}
     */
    public void hideLoading() {
        mSwRefresh.setRefreshing(false);
    }

    @Override
    public void OnclickViewItem(String link) {

    }
}