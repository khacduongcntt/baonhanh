package com.docbao.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.docbao.R;
import com.docbao.read.RssItem;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;



/**
 * Adapter for listing {@link RssItem}
 */
public class RssItemsAdapter extends RecyclerView.Adapter<RssItemsAdapter.ViewHolder> {

    private final List<RssItem> mItems = new ArrayList<>();
    private OnItemClickListener mListener;
    private final Context mContext;

    public RssItemsAdapter(Context context) {
        mContext = context;
    }

    /**
     * Item click listener
     *
     * @param listener listener
     */
    public void setListener(OnItemClickListener listener) {
        this.mListener = listener;
    }

    /**
     * Set {@link RssItem} list
     *
     * @param items item list
     */
    public void setItems(List<RssItem> items) {
        mItems.clear();
        mItems.addAll(items);
    }

    @Override
    public RssItemsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_rss_item, parent, false);
        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(RssItemsAdapter.ViewHolder holder, int position) {
        if (mItems.size() <= position) {
            return;
        }
        final RssItem item = mItems.get(position);
        holder.textTitle.setText(item.getTitle());

        String s = item.getPublishDate();
        String date = s.replace("+0700", "");


        holder.textPubDate.setText(date);

        if (item.getImage() != null) {
            Picasso.with(mContext).load(item.getImage()).
                    fit()
                    .centerCrop()
                    .into(holder.imageThumb);
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mListener != null) mListener.onItemSelected(item);
            }
        });
        holder.itemView.setTag(item);

    }


    @Override
    public int getItemCount() {
        return mItems.size();
    }


    public interface OnItemClickListener {
        void onItemSelected(RssItem rssItem);
    }

  public   static class ViewHolder extends RecyclerView.ViewHolder {

        TextView textTitle;

        TextView textPubDate;

        ImageView imageThumb;

        RelativeLayout llTextContainer;


        ViewHolder(View itemView) {
            super(itemView);
            textTitle = itemView.findViewById(R.id.tvTitle);
            textPubDate = itemView.findViewById(R.id.tvPubDate);
            imageThumb = itemView.findViewById(R.id.ivThumb);
            llTextContainer = itemView.findViewById(R.id.llTextContainer);
        }
    }
}
