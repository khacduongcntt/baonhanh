package com.docbao.model;

public class RssModel {
    String title;
    String description;
    String link;

    String pubDate;
    String thumg;

    public String getThumg() {
        return thumg;
    }

    public void setThumg(String thumg) {
        this.thumg = thumg;
    }

    public RssModel() {
    }

    public RssModel(String title, String description, String link, String pubDate, String thumg) {
        this.title = title;
        this.description = description;
        this.link = link;

        this.pubDate = pubDate;
        this.thumg = thumg;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getPubDate() {
        return pubDate;
    }

    public void setPubDate(String pubDate) {
        this.pubDate = pubDate;
    }
}
